import {SVG} from '@svgdotjs/svg.js';
import './styles.css';

const draw = SVG().addTo('body') // eslint-disable-line new-cap
    .size('100%', '100%')
    .font({family: 'FiraMono'});

draw.text('Hello Savage');
// draw.rect(100, 100).attr({fill: '#f06'});
